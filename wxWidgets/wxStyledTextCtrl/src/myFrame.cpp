#include <iostream>

#include "myApp.h"
#include "myFrame.h"

#include <wx/wx.h>
#include <wx/stc/stc.h>
#include <wx/stdpaths.h>
#include <wx/filename.h>

using namespace std;


wxBEGIN_EVENT_TABLE( myFrame, wxFrame )
    EVT_MENU( ID_File_Open,	myFrame::OnFileOpen )
    EVT_MENU( wxID_EXIT,	myFrame::OnExit )
wxEND_EVENT_TABLE()

wxIMPLEMENT_APP( myApp );

myFrame::myFrame( const wxString& title, const wxPoint& pos, const wxSize& size ) : wxFrame(NULL, wxID_ANY, title, pos, size)
{
    wxFileName f( wxStandardPaths::Get().GetUserDataDir() );
    m_user_home = f.GetPath();

	// Menus
	wxMenu *menuFile = new wxMenu;
    menuFile->Append( ID_File_Open, "&Open File\tCtrl-O", "Open source file" );
    menuFile->AppendSeparator();
    menuFile->Append( wxID_EXIT );
    // Menubar
    wxMenuBar *menuBar = new wxMenuBar;
    menuBar->Append( menuFile, "&File" );
    SetMenuBar( menuBar );

	// Status bar
    CreateStatusBar();

    // Add wxStyleTextCtrl
	wxStyledTextCtrl *sourceview = new wxStyledTextCtrl( this, wxID_ANY );
	
	
	// Style
	sourceview->StyleClearAll();
	sourceview->SetLexer( wxSTC_LEX_CPP );
	
	// Tabs
	sourceview->SetUseTabs( true );
	sourceview->SetTabWidth( 4 );
	
	
	// default spec
	// SPEC: bold turns on bold italic turns on italics fore:[name or #RRGGBB] sets the foreground colour
	// back:[name or #RRGGBB] sets the background colour face:[facename] sets the font face name to use
	// size:[num] sets the font size in points eol turns on eol filling underline turns on underlining
	wxString spec = "fore:#ffffff,back:#0f0f0f,face:SourceCode Pro,size:12";
	for( int i=0; i<64; i++ )
	{
		sourceview->StyleSetSpec( i, spec );
	}
	
	// Line numbers
	sourceview->SetMarginWidth( MARGIN_LINE_NUMBERS, 50 );
	sourceview->StyleSetForeground( wxSTC_STYLE_LINENUMBER, wxColour( 220, 220, 220) );
	sourceview->StyleSetBackground( wxSTC_STYLE_LINENUMBER, wxColour( 30, 30, 30 ) );
	sourceview->SetMarginType( MARGIN_LINE_NUMBERS, wxSTC_MARGIN_NUMBER );
	
	// code folding
	sourceview->SetMarginType( MARGIN_FOLD, wxSTC_MARGIN_SYMBOL );
	sourceview->SetMarginWidth( MARGIN_FOLD, 15 );
	sourceview->SetMarginMask( MARGIN_FOLD, wxSTC_MASK_FOLDERS );
	sourceview->StyleSetBackground( MARGIN_FOLD, wxColor( 20, 20, 20 ) );
	sourceview->SetMarginSensitive( MARGIN_FOLD, true );

	wxColor grey( 150, 150, 150 );
	sourceview->MarkerDefine( wxSTC_MARKNUM_FOLDER, wxSTC_MARK_ARROW );
	sourceview->MarkerSetForeground( wxSTC_MARKNUM_FOLDER, grey );
	sourceview->MarkerSetBackground( wxSTC_MARKNUM_FOLDER, wxColour( 30, 30, 30 ) );

	sourceview->MarkerDefine (wxSTC_MARKNUM_FOLDEROPEN,    wxSTC_MARK_ARROWDOWN);
	sourceview->MarkerSetForeground (wxSTC_MARKNUM_FOLDEROPEN, grey);
	sourceview->MarkerSetBackground (wxSTC_MARKNUM_FOLDEROPEN, wxColour( 30, 30, 30 ));
	
	sourceview->MarkerDefine (wxSTC_MARKNUM_FOLDERSUB,     wxSTC_MARK_EMPTY);
	sourceview->MarkerSetForeground (wxSTC_MARKNUM_FOLDERSUB, grey);
	sourceview->MarkerSetBackground (wxSTC_MARKNUM_FOLDERSUB, wxColour( 30, 30, 30 ));
	
	sourceview->MarkerDefine (wxSTC_MARKNUM_FOLDEREND,     wxSTC_MARK_ARROW);
	sourceview->MarkerSetForeground (wxSTC_MARKNUM_FOLDEREND, grey);
	sourceview->MarkerSetBackground (wxSTC_MARKNUM_FOLDEREND, wxColour( 30, 30, 30 ));

	sourceview->MarkerDefine (wxSTC_MARKNUM_FOLDEROPENMID, wxSTC_MARK_ARROWDOWN);
	sourceview->MarkerSetForeground (wxSTC_MARKNUM_FOLDEROPENMID, grey);
	sourceview->MarkerSetBackground (wxSTC_MARKNUM_FOLDEROPENMID, wxColour( 30, 30, 30 ));

	sourceview->MarkerDefine (wxSTC_MARKNUM_FOLDERMIDTAIL, wxSTC_MARK_EMPTY);
	sourceview->MarkerSetForeground (wxSTC_MARKNUM_FOLDERMIDTAIL, grey);
	sourceview->MarkerSetBackground (wxSTC_MARKNUM_FOLDERMIDTAIL, wxColour( 30, 30, 30 ));
	
	sourceview->MarkerDefine (wxSTC_MARKNUM_FOLDERTAIL,    wxSTC_MARK_EMPTY);
	sourceview->MarkerSetForeground (wxSTC_MARKNUM_FOLDERTAIL, grey);
	sourceview->MarkerSetBackground (wxSTC_MARKNUM_FOLDERTAIL, wxColour( 30, 30, 30 ));
	// End of code folding part
	
	// Properties found from http://www.scintilla.org/SciTEDoc.html
	sourceview->SetProperty( wxT("fold"),         wxT("1") );
	sourceview->SetProperty( wxT("fold.comment"), wxT("1") );
	sourceview->SetProperty( wxT("fold.compact"), wxT("1") );

	// Some test text
	sourceview->SetText( "#include <iostream>\n\nint main()\n{\n\tretrun 0;\n}\n" );
	
	// word wrap
	sourceview->SetWrapMode( wxSTC_WRAP_WORD );

	sourceview->SetBackgroundColour( wxColor( 10, 10, 10 ) );
	
	sourceview->StyleSetForeground (wxSTC_C_STRING,            wxColour( 220, 180, 80 ) );
	sourceview->StyleSetForeground (wxSTC_C_PREPROCESSOR,      wxColour( 200,145, 50 ) );
	sourceview->StyleSetForeground (wxSTC_C_IDENTIFIER,        wxColour( 200, 80, 255 ) );
	sourceview->StyleSetForeground (wxSTC_C_NUMBER,            wxColour( 200, 200, 50 ));
	sourceview->StyleSetForeground (wxSTC_C_CHARACTER,         wxColour( 150,150, 150 ) );
	sourceview->StyleSetForeground (wxSTC_C_WORD,              wxColour( 50, 50, 200 ) );
	sourceview->StyleSetForeground (wxSTC_C_WORD2,             wxColour( 100, 220, 100 ) );
	sourceview->StyleSetForeground (wxSTC_C_COMMENT,           wxColour( 220, 150, 150 ) );
	sourceview->StyleSetForeground (wxSTC_C_COMMENTLINE,       wxColour( 220, 150, 150 ) );
	sourceview->StyleSetForeground (wxSTC_C_COMMENTDOC,        wxColour( 150, 150, 150 ) );
	sourceview->StyleSetForeground (wxSTC_C_COMMENTDOCKEYWORD, wxColour( 100, 100,200 ) );
	sourceview->StyleSetForeground (wxSTC_C_COMMENTDOCKEYWORDERROR, wxColour( 100, 100, 220 ) );
	sourceview->StyleSetBold(wxSTC_C_WORD, true);
	sourceview->StyleSetBold(wxSTC_C_WORD2, true);
	sourceview->StyleSetBold(wxSTC_C_COMMENTDOCKEYWORD, true);

	// Selections
	sourceview->SetSelForeground( true, wxColor( 10, 10, 10 ) );
	sourceview->SetSelBackground( true, wxColor( 220, 220, 100 ) );

	// Carrot (Cursor)
	sourceview->SetCaretForeground( wxColor( 200, 200, 200 ) );

	// A sample list of keywords
	sourceview->SetKeyWords(0, wxT("class return for while switch break continue if else"));
	sourceview->SetKeyWords(1, wxT("const int float void char double"));
	
    wxBoxSizer *sizer = new wxBoxSizer( wxVERTICAL );
    sizer->Add( sourceview, 1, wxEXPAND | wxALL );
    this->SetSizer( sizer );
}

void myFrame::OnExit(wxCommandEvent& event)
{
	Close( true );
}

void myFrame::OnFileOpen( wxCommandEvent& event )
{
}

