#ifndef MYFRAME_H
#define MYFRAME_H

#include <wx/wx.h>

enum // events
{
	ID_File_Open = wxID_HIGHEST + 1
};

enum // StyleTextCtrl
{
	MARGIN_LINE_NUMBERS,
	MARGIN_FOLD
};


class myFrame : public wxFrame
{
public:
    myFrame( const wxString& title, const wxPoint& pos, const wxSize& size );

private:
    void OnExit( wxCommandEvent& event );
	void OnFileOpen( wxCommandEvent& event );
	
    wxString m_user_home;

    wxDECLARE_EVENT_TABLE();
};

#endif // MYFRAME_H

