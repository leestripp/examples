cmake_minimum_required(VERSION 3.9)

project(helloworld_gtk4 LANGUAGES C)

set(CMAKE_C_STANDARD 17)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_BUILD_TYPE Debug)

# pkg-config
find_package(PkgConfig REQUIRED)

# gtk4
pkg_check_modules(GTK REQUIRED gtk4)
include_directories(${GTK_INCLUDE_DIRS})
link_directories(${GTK_LIBRARY_DIRS})

include_directories(
    src/
    )

file(GLOB MySources
    src/*.c
)

add_executable(helloworld_gtk4
    ${MySources}
    )

target_link_libraries(helloworld_gtk4
    ${GTK_LIBRARIES}
)

