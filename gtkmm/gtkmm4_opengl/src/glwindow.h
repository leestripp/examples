#ifndef GLWINDOW_H
#define GLWINDOW_H

// GLEW
#include <GL/glew.h>

#include <gtkmm.h>
#include <glibmm.h>

using namespace std;

class glviewwindow : public Gtk::Window
{
public:
    glviewwindow();
    virtual ~glviewwindow();

    void buildUI();

    void init_buffers();
    void init_shaders();
    void draw_triangle();

private:
// Slots
    void gla_realize();
    void gla_unrealize();
    void gla_resize( int width, int height );
    bool gla_render(const Glib::RefPtr<Gdk::GLContext> &context);
    bool on_key_pressed(guint keyval, guint, Gdk::ModifierType state);
    void on_key_released(guint keyval, guint, Gdk::ModifierType state);
    bool Update(); // timer triggered render of the openGL area.

    void glInfo();

// OpenGL
    Gtk::GLArea m_GLArea;
    GLuint m_Vao;
    GLuint m_Buffers[2];
    GLuint m_Program;

// timer
    sigc::connection m_timer;
};

#endif // GLWINDOW_H
