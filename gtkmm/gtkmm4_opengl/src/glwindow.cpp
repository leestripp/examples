#include <iostream>
#include <string>

#include <gdkmm/glcontext.h>

#include "glwindow.h"

// Triangle data
static const GLfloat vertex_data[] = {
  0.0f,  0.5f,  0.0f,
  0.5f, -0.5f,  0.0f,
  -0.5f, -0.5f,  0.0f
};

// Triangle data
static const GLfloat colour_data[] = {
  1.0f, 0.0f, 0.0f,
  0.0f, 1.0f, 0.0f,
  0.0f, 0.0f, 1.0f
};


const char *vertexShaderSource = "#version 400 core\n"
    "layout (location = 0) in vec3 aPos;\n"
    "layout (location = 1) in vec3 aColour;\n"
    "out vec3 n_Colour;"
    "void main()\n"
    "{\n"
    " n_Colour = aColour;"
    " gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
    "}\0";

const char *fragmentShaderSource = "#version 400 core\n"
    "out vec4 FragColour;\n"
    "in vec3 n_Colour;"
    "void main()\n"
    "{\n"
    "   FragColour = vec4( n_Colour.r, n_Colour.g, n_Colour.b, 1.0f );\n"
    "}\n\0";

// Callback for OpenGL errors.
void GLAPIENTRY MessageCallback( GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam )
{
  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
}


// *********************************
// C++ class start
glviewwindow::glviewwindow()
{
  buildUI();

  // keyboard handlers
  auto controller = Gtk::EventControllerKey::create();
  controller->signal_key_pressed().connect(sigc::mem_fun(*this, &glviewwindow::on_key_pressed), false);
  controller->signal_key_released().connect(sigc::mem_fun(*this, &glviewwindow::on_key_released), false);
  add_controller( controller );
}

glviewwindow::~glviewwindow()
{
}


bool glviewwindow::on_key_pressed(guint keyval, guint, Gdk::ModifierType state)
{
  // Modifiers
  // state & (Gdk::ModifierType::SHIFT_MASK | Gdk::ModifierType::CONTROL_MASK | Gdk::ModifierType::ALT_MASK)

  switch( keyval )
  {
    case GDK_KEY_w:
      cout << "w pressed" << endl;
      break;

    case GDK_KEY_a:
      cout << "a pressed" << endl;
      break;

    case GDK_KEY_s:
      cout << "s pressed" << endl;
      break;

    case GDK_KEY_d:
      cout << "d pressed" << endl;
      break;

    default:
      break;
  }
  return false;
}

void glviewwindow::on_key_released(guint keyval, guint, Gdk::ModifierType state)
{
  switch( keyval )
  {
    case GDK_KEY_w:
      cout << "w released" << endl;
      break;

    case GDK_KEY_a:
      cout << "a released" << endl;
      break;

    case GDK_KEY_s:
      cout << "s released" << endl;
      break;

    case GDK_KEY_d:
      cout << "d released" << endl;
      break;

    case GDK_KEY_F1:
      if( property_fullscreened() )
      {
        unfullscreen();
      } else
      {
        fullscreen();
      }
      break;

    case GDK_KEY_Escape:
      close();
      break;

    default:
      break;
  }
}


bool glviewwindow::Update()
{
  // cout << "Update called..." << endl;

  m_GLArea.queue_render();
  return true;
}

void glviewwindow::buildUI()
{
  set_title("gtkmm_opengl");
  set_default_size(800, 600);

  //openGL settings
  m_GLArea.set_required_version( 4, 3 );    // this seems to be ignored.
  m_GLArea.set_has_depth_buffer( true );

  // Connect glarea signals
  m_GLArea.signal_realize().connect(sigc::mem_fun(*this, &glviewwindow::gla_realize));
  m_GLArea.signal_unrealize().connect(sigc::mem_fun(*this, &glviewwindow::gla_unrealize), false);
  m_GLArea.signal_resize().connect(sigc::mem_fun(*this, &glviewwindow::gla_resize), false);
  m_GLArea.signal_render().connect(sigc::mem_fun(*this, &glviewwindow::gla_render), false);

  m_GLArea.set_auto_render(false);
  set_child(m_GLArea);
}


void glviewwindow::init_buffers()
{
  glGenVertexArrays(1, &m_Vao );
  glGenBuffers( 2, &m_Buffers[0] );

  glBindVertexArray( m_Vao );

  // vertex data
  glBindBuffer( GL_ARRAY_BUFFER, m_Buffers[0] );
  glEnableVertexAttribArray( 0 );
  glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, nullptr );
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data), vertex_data, GL_STATIC_DRAW);

  // colour data
  glBindBuffer( GL_ARRAY_BUFFER, m_Buffers[1] );
  glEnableVertexAttribArray( 1 );
  glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, 0, nullptr );
  glBufferData( GL_ARRAY_BUFFER, sizeof(colour_data), colour_data, GL_STATIC_DRAW );

  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  glBindVertexArray(0);
}

static GLuint create_shader(int type, const char *src)
{
  auto shader = glCreateShader(type);
  glShaderSource(shader, 1, &src, nullptr);
  glCompileShader(shader);

  int success;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    int log_len;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);

    string log_space(log_len + 1, ' ');
    glGetShaderInfoLog(shader, log_len, nullptr, (GLchar *)log_space.c_str());

    cerr << "Compile failure in " << (type == GL_VERTEX_SHADER ? "vertex" : "fragment") << " shader: " << log_space << endl;

    glDeleteShader(shader);

    return 0;
  }

  return shader;
}

void glviewwindow::init_shaders()
{
  auto vertex = create_shader(GL_VERTEX_SHADER, vertexShaderSource);
  if (vertex == 0)
  {
    m_Program = 0;
    return;
  }

  auto fragment = create_shader(GL_FRAGMENT_SHADER,fragmentShaderSource);
  if (fragment == 0)
  {
    glDeleteShader(vertex);
    m_Program = 0;
    return;
  }

  m_Program = glCreateProgram();
  glAttachShader(m_Program, vertex);
  glAttachShader(m_Program, fragment);

  glLinkProgram(m_Program);

  int status;
  glGetProgramiv(m_Program, GL_LINK_STATUS, &status);
  if (status == GL_FALSE)
  {
    int log_len;
    glGetProgramiv(m_Program, GL_INFO_LOG_LENGTH, &log_len);

    string log_space(log_len + 1, ' ');
    glGetProgramInfoLog(m_Program, log_len, nullptr, (GLchar *)log_space.c_str());

    cerr << "Linking failure: " << log_space << endl;

    glDeleteProgram(m_Program);
    m_Program = 0;
  }

  glDeleteShader(vertex);
  glDeleteShader(fragment);
}

void glviewwindow::draw_triangle()
{
  glUseProgram(m_Program);
  glBindVertexArray(m_Vao);

  glDrawArrays( GL_TRIANGLES, 0, 3 );

  glBindVertexArray(0);
  glUseProgram(0);
}


/* *************************
 * GLArea - realize
 * *************************/

void glviewwindow::gla_realize()
{
  cout << "glviewwindow::gla_realize() - Called." << endl;

  m_GLArea.make_current();

  glewExperimental = true; // TODO: test to see if this is needed.
  if(glewInit() != GLEW_OK)
  {
      cout << "glewInit() : cannot init" << endl;
      return;
  }

  // Need debug context and extensions loaded first.
  glEnable( GL_DEBUG_OUTPUT );
  // glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS ); // msg displayed when it happens
  glDebugMessageCallback( MessageCallback, 0 );

  try
  {
    m_GLArea.throw_if_error();

    glViewport( 0, 0, m_GLArea.get_width(), m_GLArea.get_height() );

    glInfo();

    if( m_GLArea.get_context() )
    {
      cout << "Gdk::glcontext - info *******************" << endl;
      cout << "Debug context  = " << m_GLArea.get_context()->get_debug_enabled() << endl;
      cout << "Shared context = " << m_GLArea.get_context()->get_shared_context() << endl;
      cout << "is_legacy      = " << m_GLArea.get_context()->is_legacy() << endl;
    }

    init_buffers();
    init_shaders();
  }
  catch(const Gdk::GLError &gle)
  {
    cerr << "An error occured during realize:" << endl;
    cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << endl;
  }

  // Update timer
  m_timer = Glib::signal_timeout().connect( sigc::mem_fun( *this, &glviewwindow::Update ), 1000 );
}

void glviewwindow::gla_unrealize()
{
  // TODO: is this needed in gtkmm4 ?
  // Just to be safe.
  m_timer.disconnect();

  m_GLArea.make_current();

  try
  {
    m_GLArea.throw_if_error();

    // Delete buffers and shader program
    glDeleteBuffers( 2, &m_Buffers[0] );
    glDeleteVertexArrays( 1, &m_Vao );
    glDeleteProgram( m_Program );
  }
  catch(const Gdk::GLError &gle)
  {
    cerr << "An error occured making the context current during unrealize" << endl;
    cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << endl;
  }
}

void glviewwindow::glInfo()
{
    cout << "***** ******************************************" << endl;
    cout << "***** OpenGL Info" << endl;
    cout << "***** ******************************************" << endl;

    // Version
    cout << "* Version  : ";
    cout << glGetString(GL_VERSION) << endl;

    // Vender
    cout << "* Vender   : ";
    cout << glGetString( GL_VENDOR ) << endl;

    // Renderer
    cout << "* Renderer : ";
    cout << glGetString( GL_RENDERER ) << endl;

    // GL_SHADING_LANGUAGE_VERSION​​
    cout << "* Shaders  : ";
    cout << glGetString( GL_SHADING_LANGUAGE_VERSION ) << endl;

    cout << "***** ******************************************" << endl;
}

void glviewwindow::gla_resize( int width, int height )
{
  // cout << "gla_resize called : " << width << "x" << height << endl;

  m_GLArea.make_current();

  try
  {
    m_GLArea.throw_if_error();

    glViewport( 0, 0, width, height );
  }
  catch(const Gdk::GLError &gle)
  {
    cerr << "An error occured making the context current during unrealize" << endl;
    cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << endl;
  }
}

/* *************************
 * GLArea - Render
 * */
bool glviewwindow::gla_render(const Glib::RefPtr<Gdk::GLContext> &context )
{
  // cout << "gla_render called..." << endl;

  m_GLArea.make_current();

  try
  {
    m_GLArea.throw_if_error();

    float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    float g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    float b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    glClearColor( r, b, b, 1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    draw_triangle();

    // glFlush(); // docs say this is called for us after the signal call ends.

    return true;
  }
  catch(const Gdk::GLError &gle)
  {
    cerr << "An error occurred in the render callback of the GLArea" << endl;
    cerr << gle.domain() << "-" << gle.code() << "-" << gle.what() << endl;
    return false;
  }
}
