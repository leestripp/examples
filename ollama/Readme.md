## ollama examples v0.1-Alpha

# NOTE: Only tested on manjaro Linux (Linux only for now).

# ollama_gtkmm

![image](screenshots/ollama_gtkmm.png)

# Front-end to Ollama

From the ollama website:
* Get up and running with large language models, locally.
* Run Llama 2, Code Llama, and other models. Customize and create your own.

ollama_gtkmm is only a front-end. You must start the server and install the models manually. But that's very easy to do.

Starting the server
```
> ollama serve &
```

Then
```
> ollama run codellama
```
Installs and runs the codellama model.

```
>>> /bye
```
Exits the running and newly installed model.

You can now start ollama_gtkmm and choose the codellama model.

Check out the Ollama website for othe models to install.
