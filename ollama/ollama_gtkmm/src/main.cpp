#include <gtkmm/application.h>

#include "llamaWindow.h"

int main(int argc, char* argv[])
{
	auto app = Gtk::Application::create("com.gtkmm4_opengl.leestripp");
	return app->make_window_and_run<llamaWindow>(argc, argv);
}

