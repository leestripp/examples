#include "lsOllama.h"

lsOllama::lsOllama() :
	m_VBox(Gtk::Orientation::VERTICAL),
	m_Dispatcher(),
	m_WorkerThread( nullptr )
{
	//Add TextView's to ScrolledWindows
	m_ScrolledWindow1.set_child( m_textview );
	m_ScrolledWindow2.set_child( m_textinput );
	m_ScrolledWindow2.set_size_request( -1, 200 );
	
	m_VBox.set_margin( 10 );
	m_VBox.append( m_combo );
	m_VBox.append( m_ScrolledWindow1 );
	m_VBox.append( m_progress );
	m_VBox.append( m_ScrolledWindow2 );
	m_textinput.set_margin_top( 10 );
	// Buttons
	auto b_hbox = Gtk::make_managed<Gtk::Box>( Gtk::Orientation::HORIZONTAL, 5 );
	b_hbox->append( m_submit );
	b_hbox->append( m_clear_history );
	m_VBox.append( *b_hbox );
	
	// Set m_textview defaults
	m_textview.set_expand();
	m_textview.property_wrap_mode() = Gtk::WrapMode::WORD;
	m_textview.property_justification() = Gtk::Justification::LEFT;
	m_textview.set_editable( false );
	
	// Set m_textinput defaults
	m_textinput.set_size_request( -1, 200 );
	m_textinput.set_hexpand( true );
	m_textinput.set_vexpand( false );
	m_textinput.property_wrap_mode() = Gtk::WrapMode::WORD;
	m_textinput.property_justification() = Gtk::Justification::LEFT;
	
	// Submit Button
	auto label = Gtk::make_managed<Gtk::Label>("Submit");
	label->set_expand(true);
	//Put them in a Box:
	auto hbox = Gtk::make_managed<Gtk::Box>(Gtk::Orientation::HORIZONTAL, 5);
	hbox->append( *label );
	//And put that Box in the button:
	m_submit.set_child( *hbox );
	m_submit.set_hexpand( true );
	m_submit.set_vexpand( false );
	
	// m_clear_history Button
	auto label_ch = Gtk::make_managed<Gtk::Label>( "Clear History" );
	label_ch->set_expand( true );
	auto hbox_ch = Gtk::make_managed<Gtk::Box>( Gtk::Orientation::HORIZONTAL, 5 );
	hbox_ch->append( *label_ch );
	m_clear_history.set_child( *hbox_ch );
	m_clear_history.set_hexpand( true );
	m_clear_history.set_vexpand( false );
	
	// Combo Models
	m_combo.append( "codellama" );
	m_combo.append( "llama2" );					// Meta
	m_combo.append( "orca2" );					// Microsoft
	m_combo.append( "screenplayAI" );
	m_combo.set_active(0);
	
	// progress bar
	m_progress.set_valign(Gtk::Align::CENTER);
	m_progress.set_size_request(100, -1);
	m_progress.set_text( "What's on your mind" );
	m_progress.set_show_text( true );
	
	append( m_VBox );
	
	// Signals
	m_submit.signal_clicked().connect( sigc::mem_fun( *this, &lsOllama::on_submit_clicked ) );
	m_clear_history.signal_clicked().connect( sigc::mem_fun( *this, &lsOllama::on_clear_history_clicked ) );
	m_combo.signal_changed().connect( sigc::mem_fun( *this, &lsOllama::on_combo_changed ) );
	// Connect the handler to the dispatcher.
	m_Dispatcher.connect( sigc::mem_fun( *this, &lsOllama::on_notification_from_worker_thread ) );
}

lsOllama::~lsOllama()
{
	if( m_WorkerThread)
	{
		// TODO: Add stop feature
		// m_curl.stop_work();
		if( m_WorkerThread->joinable() ) m_WorkerThread->join();
	}
}


void lsOllama::on_submit_clicked()
{
	// Disable the buttons
	m_clear_history.set_sensitive( false );
	m_submit.set_sensitive( false );
	m_progress.set_text( "Processing..." );
	
	// Timer
	sigc::slot<bool()> my_slot = sigc::bind( sigc::mem_fun( *this, &lsOllama::on_timeout) );
	m_timer = Glib::signal_timeout().connect( my_slot, 1000 );
	
	// Check model
	string model = m_combo.get_active_text();
	if( model == "" )
	{
		// fallback
		model = "llama2";
	}
	
	auto input_buffer = m_textinput.get_buffer();
	
	// set global vars for Thread access.
	m_text = input_buffer->get_text();
	m_model = model;
	
	// Submit as thread
	if( m_WorkerThread )
	{
		cout << "FIX THIS : Disable submit button" << endl;
	} else
	{
		// Start a new worker thread.
		m_WorkerThread = new std::thread( [this]
		{
			m_curl.submit( this );
		} );
	}
}


void lsOllama::on_clear_history_clicked()
{
	m_context = "";
	m_textview.get_buffer()->set_text( "" );
	m_textinput.get_buffer()->set_text( "" );
}

void lsOllama::notify()
{
	// trigger call to - on_notification_from_worker_thread()
	m_Dispatcher.emit();
}


void lsOllama::on_notification_from_worker_thread()
{
	if( m_WorkerThread && ! m_curl.is_running() )
	{
		// Work is done.
		if( m_WorkerThread->joinable() ) m_WorkerThread->join();
		delete m_WorkerThread;
		m_WorkerThread = nullptr;
		
		// output responce
		auto output_buffer = m_textview.get_buffer();
		// Create a mark that "points" to the end of the buffer.
        auto endMark = output_buffer->create_mark( output_buffer->end(), false );
		output_buffer->insert( output_buffer->end(), "\n\n## " + m_model + "\n" );
		output_buffer->insert( output_buffer->end(), m_curl.m_response );
		// Scroll the view to the end of the buffer
		m_textview.scroll_to( endMark );
		output_buffer->delete_mark( endMark );
	}
	
	m_timer.disconnect();
	m_progress.set_text( "What's on your mind" );
	m_progress.set_fraction( 0.0 );
	// Enable buttons
	m_submit.set_sensitive( true );
	m_clear_history.set_sensitive( true );
}


void lsOllama::on_combo_changed()
{
	// model changed so clear history.
	on_clear_history_clicked();
}


bool lsOllama::on_timeout()
{
	//cout << "Timeout ***" << endl;
	m_progress.pulse();
	
	return true;
}


