#include "lsCURL.h"
#include "lsOllama.h"

lsCURL::lsCURL()
{
	// thread info
	m_running = false;
	
	curl_global_init( CURL_GLOBAL_DEFAULT );
}

lsCURL::~lsCURL()
{
	curl_global_cleanup();
}


void lsCURL::submit( lsOllama *caller )
{
	m_running = true;
	
	// Clean CURL
	m_curl = curl_easy_init();
	if(! m_curl )
	{
		cerr << "curl_easy_init() - failed" << endl;
		m_running = false;
		caller->notify();
		return;
	}
	
	// clear the buffer
	m_output = "";
	
	// cleanup string
	string input = caller->m_text;
	for( string::size_type i = 0; i < input.size(); i++ )
	{
		if( input[i] == '\n' || input[i] == '\r' ) input[i] = ' ';
		if( input[i] == '\t' ) input[i] = ' ';
		if( input[i] == '"' ) input[i] = '\'';
	}
	
	// Curl settings
	curl_easy_setopt( m_curl, CURLOPT_URL, "http://localhost:11434/api/generate" );
	curl_easy_setopt( m_curl, CURLOPT_WRITEFUNCTION, this->writefunc );
	curl_easy_setopt( m_curl, CURLOPT_WRITEDATA, this );
	curl_easy_setopt( m_curl, CURLOPT_SSL_VERIFYPEER, 0L );
	curl_easy_setopt( m_curl, CURLOPT_SSL_VERIFYHOST, 0L );
	// cache the CA cert for a week
	curl_easy_setopt( m_curl, CURLOPT_CA_CACHE_TIMEOUT, 604800L );
	
	// data
	string data = "{";
	data += "\"model\": \"" + caller->m_model + "\",";
	data += "\"prompt\": \"" + input + "\", ";
	data += "\"stream\": false";
	if( caller->m_context != "" ) data += ", \"context\": " + caller->m_context;
	data += "}";
	curl_easy_setopt( m_curl, CURLOPT_POSTFIELDS, data.c_str() );
	
	// debug
	cout << "\n\n  ***** DATA: " << data << endl;
	
	m_multi_handle = curl_multi_init();
	if( m_multi_handle )
	{
		curl_multi_add_handle( m_multi_handle, m_curl );
	} else
	{
		cerr << "curl_multi_init() - failed" << endl;
		
		// clean up
		if( m_curl )
		{
			curl_easy_cleanup( m_curl );
		}
		m_running = false;
		caller->notify();
		return;
	}
	
	// Perform the request
	int still_running;
	do {
		CURLMcode mc;
		
		mc = curl_multi_perform( m_multi_handle, &still_running );
		if(! mc && still_running )
		{
			/* wait for activity, timeout or "nothing" */
			mc = curl_multi_poll( m_multi_handle, NULL, 0, 1000, NULL );
		}
		
		if( mc )
		{
			cerr << "curl_multi_poll() failed : " << curl_multi_strerror( mc ) << endl;
			break;
		}
	} while( still_running );
	
	// clean up
	if( m_multi_handle )
	{
		curl_multi_remove_handle( m_multi_handle, m_curl );
		curl_multi_cleanup( m_multi_handle );
	}
	
	if( m_output == "" )
	{
		m_response = "Ollama : model not responding, check server is running.\n\n";
		cerr << m_response << endl;
	} else
	{
		// debug
		cout << "\n\n  ***** Return buffer - m_output : " << m_output << endl;
		
		// process the result
		// JSON
		Json::Value values;
		Json::Reader reader;
		reader.parse( m_output, values );
		// Get the response from the JSON object
		if( values["response"] )
		{
			m_response = values["response"].asString();
		} else
		{
			// Do nothing
		}
		
		// extract context history
		// Check if a specific key exists in the object
		if( values["context"] )
		{
			Json::StreamWriterBuilder builder;
			builder.settings_["indentation"] = "";
			caller->m_context = Json::writeString( builder, values["context"] );
		} else
		{
			// Do nothing
		}
		// debug
		cout << "\n\n  ***** m_context : " << caller->m_context << endl;
	}
	
	// clean up
	if( m_curl )
	{
		curl_easy_cleanup( m_curl );
	}
	
	// Tell main thread we are done.
	m_running = false;
	caller->notify();
}

bool lsCURL::is_running()
{
	return m_running;
}

// STATIC Callback

size_t lsCURL::writefunc( void *ptr, size_t size, size_t nmemb, lsCURL *curl )
{
	// output progress to Term
	string s;
	s.append( (char *)ptr, size*nmemb );
	cout << s;
	
	// collect full JSON output.
	curl->m_output.append( (char *)ptr, size*nmemb );
	return size*nmemb;
}

