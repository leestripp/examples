#ifndef LSOLLAMA_H
#define LSOLLAMA_H

#include <thread>
#include <iostream>
#include <gtkmm.h>

// lsCURL
#include "lsCURL.h"

using namespace std;


class lsOllama : public Gtk::Box
{
public:
	lsOllama();
	virtual ~lsOllama();
	
	// Thread calls this to update.
	void notify();
	
	// global vars for Thread.
	string m_text, m_model;
	string m_context;
	
private:
	void on_submit_clicked();
	void on_clear_history_clicked();
	void on_combo_changed();
	// Dispatcher handler.
	void on_notification_from_worker_thread();
	// timer
	bool on_timeout();
	
	// widgets
	Gtk::Box m_VBox;
	Gtk::ScrolledWindow m_ScrolledWindow1;
	Gtk::ScrolledWindow m_ScrolledWindow2;
	Gtk::TextView m_textview;
	Gtk::TextView m_textinput;
	Gtk::Button m_submit;
	Gtk::Button m_clear_history;
	Gtk::ComboBoxText m_combo;
	Gtk::ProgressBar m_progress;
	
	// Threads
	Glib::Dispatcher m_Dispatcher;
	thread *m_WorkerThread;
	// CURL - worker
	lsCURL m_curl;
	
	//timer
	sigc::connection m_timer;
};

#endif // LSOLLAMA_H