#ifndef LSCURL_H
#define LSCURL_H

#include <algorithm>
#include <iostream>
#include <iterator>
#include <curl/curl.h>
#include <json/json.h>
#include <gtkmm.h>
#include <thread>

// proto
class lsOllama;

using namespace std;


class lsCURL
{
public:
	lsCURL();
	virtual ~lsCURL();
	
	void submit( lsOllama *caller );
	bool is_running();
	
	// buffers
	string m_output;
	string m_response;
	
private:
	static size_t writefunc( void *ptr, size_t size, size_t nmemb, lsCURL *curl );
	bool m_running;
	CURL *m_curl;
	CURLM *m_multi_handle;
};

#endif // LSCURL_H