#ifndef LLAMAWINDOW_H
#define LLAMAWINDOW_H

#include <iostream>
#include <gtkmm.h>
#include "ollama/lsOllama.h"

using namespace std;

class llamaWindow : public Gtk::Window
{
public:
	llamaWindow();
	virtual ~llamaWindow();
	
private:
	Gtk::Box m_VBox;
	lsOllama m_lsollama;
};


#endif // LLAMAWINDOW_H