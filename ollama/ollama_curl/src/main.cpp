#include <iostream>
#include <curl/curl.h>
#include <json/json.h>

using namespace std;

// globals
string output;

size_t writefunc( void *ptr, size_t size, size_t nmemb )
{
	output.append( (char *)ptr, size*nmemb );
	return size*nmemb;
}

int main()
{
	CURLcode res;
	
	curl_global_init( CURL_GLOBAL_DEFAULT );
	
	CURL *curl = curl_easy_init();
	if( curl )
	{
		// Curl
		curl_easy_setopt( curl, CURLOPT_URL, "http://localhost:11434/api/generate" );
		curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, writefunc );
		curl_easy_setopt( curl, CURLOPT_SSL_VERIFYPEER, 0L );
		curl_easy_setopt( curl, CURLOPT_SSL_VERIFYHOST, 0L );
		/* cache the CA cert bundle in memory for a week */
		curl_easy_setopt( curl, CURLOPT_CA_CACHE_TIMEOUT, 604800L );
		
		while( true )
		{
			string input;
			
			cout << ">>> ";
			getline( cin, input );
			
			if( input.compare("quit") == 0 )
				break;
			
			output = "";
			
			// data
			string data = "{";
			data += "\"model\": \"llama2\",";
			data += "\"prompt\": \"" + input + "\",";
			data += "\"stream\": false";
			data += "}";
			curl_easy_setopt( curl, CURLOPT_POSTFIELDS, data.c_str() );
			
			/* Perform the request, res will get the return code */
			res = curl_easy_perform( curl );
			
			if( res != CURLE_OK )
			{
				cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << endl;
			}
			
			cout << endl << endl;
			cout << "  llama  ***************************************" << endl;
			
			// JSON
			Json::Value values;
			Json::Reader reader;
			reader.parse( output, values );
			
			// Get the response from the JSON object
			string response = values["response"].asString();
			
			// Print the response to the console
			cout << response << endl << endl;
			
		} // while

		/* always cleanup */
		curl_easy_cleanup(curl);
	}// if curl
	curl_global_cleanup();
	
	return 0;
}
