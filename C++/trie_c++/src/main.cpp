#include <iostream>
#include "lsTrie.h"


int main()
{
	lsTrie root;
	
	// load file
	string contents;
	cout << "Loading test file" << endl;
	
	contents = root.loadFile( "../src/lsTrie.cpp" );
	
	contents = root.removeComments( contents );
	cout << "Remove comments : **********************" << endl;
	cout << contents << endl;
	
	vector<string> list;
	list = root.regex_split( contents );
	for( auto word: list )
	{
		root.insert( word );
	}
	root.insert( "cat" );
	root.insert( "cats" );

	// Find prefix
	root.findPrefix( "ca" );
	root.findPrefix( "wh" );
	root.findPrefix( "ls" );
	root.findPrefix( "f" );
	// cleanup
	root.clearTree();
	
	cout << endl << "Basic numbers test ***" << endl;
	root.insert( "1" );
	root.insert( "12" );
	root.insert( "123" );
	root.insert( "1234" );
	root.insert( "12345" );
	// list all words
	root.iterate();
	root.clearTree();
	
	cout << endl << "Basic Same word test ***" << endl;
	root.insert( "Same" );
	root.insert( "Same" );
	root.insert( "Same" );
	root.insert( "Same" );
	root.insert( "Same" );
	// list all words
	root.iterate();
	root.clearTree();
	
	return 0;
}

