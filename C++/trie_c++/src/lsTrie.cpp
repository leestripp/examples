/*
 *
 * Used as test file
**/

#include "lsTrie.h"

lsTrie::lsTrie()
{
	root = NULL;
}

void lsTrie::insert( string word )
{
	cout << "Insert Word: " << word << endl;
	
	if(! root) root = new trieNode();

	trieNode *tmp = root;
	for( int i=0; i<word.length(); i++ )
	{
		if( tmp->node.find( word[i] ) == tmp->node.end() )
		{
			// No letter found, so store it and add new Node.
			tmp->node[ word[i] ] = new trieNode();
		}
		// next node.
		tmp = tmp->node[ word[i] ];
    }
    // whole word added
	tmp->isWord = true;
}

void lsTrie::iterate_( const trieNode* trienode, const std::string& prefix )
{
	if( trienode->isWord )
	{
		cout << prefix << endl;
	}
  
	for( const auto& [c, child] : trienode->node )
	{
		iterate_( child, prefix + c );
	}
}

void lsTrie::iterate()
{
	if( root )
	{
		cout << "Display all Words" << endl;
		iterate_( root, "" );
	} else
	{
		cout << "Tree empty..." << endl;
	}
}

void lsTrie::findPrefix_( const trieNode* node, const string& prefix, int pos )
{
	trieNode *tmp;
	
	if( auto search = node->node.find( prefix[pos] ); search != node->node.end() )
	{
		tmp = search->second;
		
		if( prefix.size() == pos+1 )
		{
			found = tmp;
			return;
		} else
		{
			pos++;
			findPrefix_( tmp, prefix, pos );
		}
	}
}

void lsTrie::findPrefix( const string& prefix )
{
	found = NULL;
	int pos = 0;
	
	if( root )
	{
		cout << "Find words with prefix: " << prefix << endl;
		findPrefix_( root, prefix, pos );
		
		// display words from node found.
		if( found ) iterate_( found, prefix );
		
	} else
	{
		cout << "Tree empty..." << endl;
	}
}

void lsTrie::clear_( const trieNode* trienode )
{
	for( const auto& [c, child] : trienode->node )
	{
		cout << " " << c;
		clear_( child );
		delete child;
	}
}

void lsTrie::clearTree()
{
	if( root )
	{
		cout << "clearNodes:";
		clear_( root );
		delete root;
		root = NULL;
		cout << endl;
	}
}

vector<string> lsTrie::regex_split( const string &s, const regex& sep_regex )
{
	sregex_token_iterator iter( s.begin(), s.end(), sep_regex, -1 );
	sregex_token_iterator end;

	return {iter, end};
}


string lsTrie::loadFile( const string& file )
{
	stringstream ss;
	
	ifstream myfile( file );
	if( myfile.is_open() )
	{
		ss << myfile.rdbuf() << endl;
	}
	return ss.str();
}


string lsTrie::removeComments( const string& input )
{
	bool record = true;
	bool singleline = false;
	string output, output2, buffer;
	
	for( int i=0; i < input.length(); i++)  
	{
		buffer = buffer + input[i];
		
		if(( buffer == "//" )&&(! singleline  ))
		{
			singleline = true;
			record = false;
			output.pop_back();
		}
		
		if( singleline )
		{
			if( ( input[i] == '\n' )||( input[i] == '\r' ) )
			{
				singleline = false;
				record = true;
			}
		}
		
		// buffer slide
		if( buffer.length() > 1 )
		{
			buffer[0] = buffer[1];
			buffer.pop_back();
		}
		
		if( record )
		{
			output = output + input[i];
		}
	} // for
	
	buffer = "";
	for( int i=0; i < output.length(); i++)  
	{
		buffer = buffer + output[i];
		
		if( buffer == "/*" )
		{
			record = false;
		}
		
		if( buffer == "*/" )
		{
			record = true;
		}
		
		// buffer slide
		if( buffer.length() > 1 )
		{
			buffer[0] = buffer[1];
			buffer.pop_back();
		}
		
		if( record )
		{
			output2 = output2 + output[i];
		}
	}
	
	return output2;
}

// end test
