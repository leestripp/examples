#ifndef LSTRIE_H
#define LSTRIE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <regex>

using namespace std;

// Data
class trieNode
{
public:
	unordered_map<char, trieNode*> node;
	bool isWord;
};

// Root
class lsTrie
{
public:
	lsTrie();
	
	// trie tree
	void insert( string word );
	void iterate();
	void clearTree();
	void findPrefix( const string& prefix );
	// Strings
	vector<string> regex_split( const string &s, const regex& sep_regex = regex{"[\\s.,&\"();:{}\t=\\-\\+\\[\\]<>]+"} );
	//files
	string loadFile( const string& file );
	string removeComments( const string& input );
	
private:
	void iterate_( const trieNode* node, const string& prefix );
	void clear_( const trieNode* trienode );
	void findPrefix_( const trieNode* node, const string& prefix, int pos );
	
	trieNode *root;
	trieNode *found;
};

#endif // LSTRIE_H

