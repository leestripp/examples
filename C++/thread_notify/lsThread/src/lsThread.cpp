#include "lsThread.h"

lsThread::lsThread( int tt_type )
{
	m_worker = NULL;
	
	m_type = tt_type;
	m_done = false;
}

lsThread::~lsThread()
{
	if( m_worker ) delete m_worker;
}


void lsThread::start()
{
	// Start the work thread.
	m_worker = new thread( &lsThread::work, this );
}

void lsThread::stop()
{
	if( m_worker )
	{
		m_worker->join();
	}
}
