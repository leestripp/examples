#ifndef LSTHREAD_H
#define LSTHREAD_H

#include <iostream>
#include <thread>
#include <mutex>
// sigc++
#include <sigc++/sigc++.h>

using namespace std;

class lsThread : public sigc::trackable
{
public:
	lsThread( int tt_type );
	virtual ~lsThread();
	
	// Starts the work thread.
	void start();
	void stop();
	
	virtual void work() = 0;
	
	// signals.
	sigc::signal<void(lsThread *)> notify;
	
	// get set
	ulong get_type()
	{
		return m_type;
	}
	
	bool get_done()
	{
		return m_done;
	}
	
	void set_done( bool val )
	{
		m_done = val;
	}
	
private:
	thread *m_worker;
	
	ulong m_type;
	bool m_done;
};

#endif // LSTHREAD_H