#include "myThread.h"

myThread::myThread( int tt_type ) : lsThread( tt_type )
{
}


void myThread::work()
{
	// do work
	cout << "Thread: starting work..." << endl;
	for( int i=0; i<10; i++ )
	{
		cout << "Thread Loop." << endl;
		ss << i;
		this_thread::sleep_for( chrono::seconds(1) );
	}
	ss << endl;
	
	// signal we are done.
	set_done(true);
	notify.emit( this );
}

void myThread::print()
{
	cout << "Count Buffer: " << ss.str() << endl;
}
