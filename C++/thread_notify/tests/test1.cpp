#include <iostream>

// myApp
#include "myApp.h"


int main()
{
	myApp app;
	
	app.run();
	
	return 0;
}
