#ifndef MYAPP_H
#define MYAPP_H

#include <iostream>
#include <chrono>

#include "myThread.h"

// Thread types
const int LSTT_NONE		= 0;
const int LSTT_COUNT		= 1;


class myApp
{
public:
	myApp();
	
	void run();
	
	// Slots
	void notify( lsThread *th );
	
private:
	bool m_running;
};

#endif // MYAPP_H
