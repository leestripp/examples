#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <iostream>
#include <sstream>
#include <chrono>

#include "lsThread.h"

using namespace std;

class myThread : public lsThread
{
public:
	myThread( int tt_type );
	
	// override
	void work();
	
	void print();
	
private:
	stringstream ss;
};

#endif // MYTHREAD_H
