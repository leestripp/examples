#include "myApp.h"

myApp::myApp()
{
	m_running = true;
}

void myApp::run()
{
	// Create thread
	myThread th( LSTT_COUNT );
	th.notify.connect( sigc::mem_fun( *this, &myApp::notify) );
	th.start();
	
	// main loop
	while( m_running )
	{
		cout << "While loop." << endl;
		this_thread::sleep_for( chrono::seconds(1) );
	}
	
	// Joins the thread.
	th.stop();
	
	// print buffer
	th.print();
}

void myApp::notify( lsThread *th )
{
	cout << "Got notify from thread" << endl;
	
	switch( th->get_type() )
	{
		case LSTT_COUNT:
		{
			cout << "Thread type: LSTT_COUNT" << endl;
			break;
		}
		
		case LSTT_NONE:
		{
			cout << "Thread type: LSTT_NONE" << endl;
			break;
		}
		
		default:
		{
			cout << "Unknown Thread type..." << endl;
			break;
		}
	}
	
	m_running = false;
}
